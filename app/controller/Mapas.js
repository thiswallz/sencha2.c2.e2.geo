Ext.define('Ejemplo2Geo.controller.Mapas', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
            mimapa: 'mimapa',
            main: 'main'        
        },
        control: {
            'button[action=btnLocate]': {
                tap: 'locate'
            }      
        }
    },
    launch: function() {
        //console.log(this);
    },
    locate: function(){
        var view = this.getMimapa();
        var map = view.getMap();
        navigator.geolocation.getCurrentPosition(function(position){  
            if(view.mark){
                view.mark.setMap(null);   
            }                 
            view.mark = new google.maps.Marker({
                position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                map: map,
                draggable: false
            })
        }, onError);


    }
});