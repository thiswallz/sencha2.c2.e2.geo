Ext.define('Ejemplo2Geo.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar',
        'Ext.Video'
    ],
    config: {
        tabBarPosition: 'bottom',

        items: [{
            xtype : 'toolbar',
            docked: 'top',
            items: [
            {
                xtype: 'button',
                ui: 'action',
                action: 'btnLocate',
                iconMask: true,
                iconCls: 'locate'
            }]
        },
       {
            title: 'Lista',
            iconCls: 'action',
            items: [
                {
                    xtype: 'mimapa'               
                }
            ]
        }]
    }
});
