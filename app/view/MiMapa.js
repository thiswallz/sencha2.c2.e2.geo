Ext.define('Ejemplo2Geo.view.MiMapa', {
    extend: 'Ext.Map',
    alias: 'widget.mimapa',

    config: {
        mark: null,
        useCurrentLocation: true,
        fullscreen : true,        
        scrollable: true,
        centered: true,
        width: '100%',
        height: '100%',
        listeners: [
            {
                fn: 'onMapMaprender',
                event: 'maprender'
            }
        ]
    },
    onMapMaprender: function(map, gmap, options) {

    }

});